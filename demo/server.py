import asyncio
import logging
import random

from contextlib import suppress

import rebelrpc
from .common import PROTO_PATH


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')


class Handler:
    async def filter_invoices(self, stream):
        message = await stream.receive_message()
        logging.info(f"Received message: {message}")

        for _ in range(3):
            await stream.send_message({
                "id": random.randint(1, 1000),
                "status": "new",
                "order_id": str(random.randint(1, 10000)),
                "amount": random.random() * 100,
                "currency": "USD",
            })
            # await asyncio.sleep(3)
            # if _ == 1:
            #     raise RuntimeError("A")

    async def create_invoice(self, stream):
        message = await stream.receive_message()
        logging.info(f"Received message: {message}")

        fee = random.random()
        if message['order_id'].startswith('000'):
            fee = 0.

        await stream.send_message({
            "id": random.randint(1, 1000),
            "fee": fee,
        })

    async def ping(self, stream):
        while True:
            message = await stream.receive_message()
            if not message:
                break

            logging.info(f"Received message {message}")

            resp = {}
            if message.get('check_db_connection'):
                resp['db_connection_status'] = True

            if message.get('check_redis_connection'):
                resp['redis_connection_status'] = True

            await stream.send_message(resp)


async def _check_auth_header_middleware(stream, continuation):
    auth_header = stream.get_request_header("Auth", "")
    if auth_header != "Blya boodoo":
        await stream.send_error(403, {"code": "YA_VAS_NE_ZNAYU", "message": "Go nahooi"})
        return

    await continuation(stream)


def main():
    loop = asyncio.get_event_loop()

    server = rebelrpc.Server(
        Handler, rebelrpc.parse_spec_file(PROTO_PATH),
        loop=loop, middlewares=[_check_auth_header_middleware],
        timeout=1,
    )

    host, port = '127.0.0.1', 50051
    loop.run_until_complete(server.start(host, port))
    logging.info('Serving on {}:{}'.format(host, port))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.stop()

    pending = asyncio.Task.all_tasks()
    for task in pending:
        task.cancel()
        with suppress(asyncio.CancelledError):
            loop.run_until_complete(task)

    loop.close()


if __name__ == '__main__':
    main()
