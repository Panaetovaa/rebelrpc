import asyncio
import logging

import rebelrpc
from rebelrpc.exceptions import BadStatusError

from .common import PROTO_PATH


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')


async def main():
    client = rebelrpc.Client(
        spec=rebelrpc.parse_spec_file(PROTO_PATH),
        middlewares=[_set_auth_header_middleware],
    )

    # ------------------------------------------------------------------------
    # UNARY_STREAM RPC

    print('\n### UNARY_STREAM')

    print(await client.filter_invoices(
        {
            'filters': {'status': ['accepted']},
            'page': 0,
            'page_size': 10,
        },
        headers={'test-name': 'test-value'},
        timeout=1,
    ))

    # This block performs the same UNARY_STREAM interaction as above
    # while showing more advanced stream control features.
    async with client.filter_invoices.connect(timeout=300, headers={'test-name': 'test-value'}) as stream:
        await asyncio.sleep(10)
        await stream.send_message({
            'filters': {'status': ['accepted']},
            'page': 0,
            'page_size': 10,
        })

        replies = [reply async for reply in stream.receive_all_messages()]
        logging.info(f"Replies: {replies}")

    # ------------------------------------------------------------------------
    # UNARY_UNARY RPC
    print('\n### Demonstrating UNARY_UNARY')

    print(await client.create_invoice({
        'amount': 100,
        'currency': 'USD',
        'order_id': '1000123',
    }, timeout=2))

    # This block performs the same UNARY_UNARY interaction as above
    # while showing more advanced stream control features.
    async with client.create_invoice.connect() as stream:
        await stream.send_message({
            'amount': 100,
            'currency': 'USD',
            'order_id': '1000123',
        })

        reply = await stream.receive_message()
        print(reply)

    # ------------------------------------------------------------------------
    # STREAM_STREAM RPC

    print('\n### Demonstrating STREAM_STREAM')

    # Demonstrate simple case where requests are known before interaction
    msgs = [
        dict(check_db_conneciton=True),
        dict(check_redis_connection=False),
        dict(check_db_connection=True, check_redis_connection=True),
    ]
    print(await client.ping(msgs))

    async with client.ping.connect() as stream:
        await stream.send_message({
            'check_db_connection': True,
        })
        reply = await stream.receive_message()
        print(reply)

        await stream.send_message({
            'check_redis_connection': True,
        })

        reply = await stream.receive_message()
        print(reply)

    # ------------------------------------------------------------------------
    # MIDDLEWARES

    print('\n### Demonstrating middlewares')
    # Demonstrate case where client not sending Auth header. Server has middleware
    # for checking it. If there is no header, then 403 error will be returned.
    # In all previous examples we were sending Auth header with client
    # middleware _set_auth_header_middleware. Now we will create new channel without middlewares.

    client = rebelrpc.Client(
        spec=rebelrpc.parse_spec_file(PROTO_PATH),
    )

    try:
        await client.ping([{'check_redis_connection': True}])
    except BadStatusError as e:
        logging.warning(f"Client replied with status != 200: {e}")

    # Now we will send header manually and check that server replied with status 200.
    reply = await client.ping([{'check_redis_connection': True}], headers={"Auth": "Blya boodoo"})
    assert reply.http_status == 200
    print(f"Server replied with {reply}")


async def _set_auth_header_middleware(stream, continuation):
    stream.update_request_headers({"Auth": "Blya boodoo"})
    await continuation(stream)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
