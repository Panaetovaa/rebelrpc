import os.path


def _abspath(path):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), path)


PROTO_PATH = _abspath('proto/billing.json')
