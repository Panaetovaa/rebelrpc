from setuptools import setup, find_packages

setup(
    name='rebelrpc',
    version='0.1.0',
    description='Pure-Python RebelRPC implementation, based on hyper-h2 project',
    author='Alexey Panaetov',
    author_email='panaetovaa@gmail.com',
    url='https://gitlab.com/',
    packages=find_packages(),
    license='BSD',
    python_requires='>=3.6',
    install_requires=[
        'h2==3.0.1',
        'multidict==4.4.2',
        'async-generator==1.9',
        'async-timeout==3.0.1',
        'jsonref==0.2',
        'jsonschema==3.0.1',
    ],
    extras_require={
        'tests': [
            'pytest==3.6.1',
            'pytest-cov>=2.2.1',
            'pytest-asyncio==0.8.0',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP :: HTTP Servers',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
