import asyncio
import functools
import logging
import socket

from asyncio import Event
from asyncio import Protocol as BaseProtocol

import h2.connection
import h2.config
import h2.events
import h2.exceptions

from async_timeout import timeout as with_timeout

from rebelrpc.exceptions import StreamTerminatedError
from rebelrpc.loggers import diagnostics_logger
from rebelrpc.stream import Stream


logger = logging.getLogger(__name__)


def raise_if_stream_closed(func):
    @functools.wraps(func)
    async def _func(self, stream_id, *args, **kwargs):
        if not self.is_stream_alive(stream_id):
            logger.warning(
                f"Method {func} cannot be processed because "
                f"stream {self.connection_id}/{stream_id} is closed."
            )
            raise StreamTerminatedError(stream_id)

        return await func(self, stream_id, *args, **kwargs)

    return _func


class Protocol(BaseProtocol):
    stream_class = Stream

    def __init__(self, h2_config, *, loop=None):
        self.connection_id = self._generate_connection_id()
        self.transport = None

        self.h2_engine = h2.connection.H2Connection(h2.config.H2Configuration(
            **h2_config
        ))

        self.streams = {}

        self.h2_events_processors = {
            h2.events.DataReceived: self._ignore_if_stream_closed(
                self.process_data_frame_received
            ),
            h2.events.StreamEnded: self._ignore_if_stream_closed(
                self.process_stream_ended
            ),
            h2.events.StreamReset: self._ignore_if_stream_closed(
                self.process_stream_reset
            ),
            h2.events.ConnectionTerminated: self.process_connection_terminated,
            h2.events.WindowUpdated: self._ignore_if_stream_closed(
                self.process_window_updated
            ),
        }

        self.loop = loop or asyncio.get_event_loop()
        self.lost = False

    def connection_made(self, transport):
        logger.debug(f"[connection:{self.connection_id}] Client connected")

        _set_transport_socket_nodelay(transport)
        self.transport = TransportWrapper(transport)

        self.h2_engine.initiate_connection()
        asyncio.ensure_future(self.flush(), loop=self.loop)

    def connection_lost(self, exc):
        logger.info(f"[connection:{self.connection_id}] Client disconnected")
        self.lost = True

    def data_received(self, data):
        asyncio.ensure_future(
            self.process_data_received(data), loop=self.loop
        )

    def pause_writing(self):
        self.transport.pause()

    def resume_writing(self):
        self.transport.resume()

    def close(self):
        if self.transport:
            self.transport.close()

    def closed(self):
        return self.transport and self.transport.closed()

    async def process_data_received(self, data):
        try:
            events = self.h2_engine.receive_data(data)
        except h2.exceptions.ProtocolError:
            logger.exception(
                f"[connection:{self.connection_id}] "
                f"Cannot parse h2 events. I am to close socket."
            )
            self.transport.close()
            raise

        await self.flush()

        for event in events:
            logger.debug(
                f"[connection:{self.connection_id}] "
                f"Received event: {event}"
            )
            proc = self.h2_events_processors.get(event.__class__)
            if not proc:
                logger.debug(
                    f"[connection:{self.connection_id}] "
                    f"Skipping event {event}"
                )
            else:
                asyncio.ensure_future(proc(event))

    async def process_data_frame_received(self, event):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {event.stream_id} received data frame."
        )
        size = len(event.data)
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {event.stream_id}: acked {size} bytes"
        )
        self.h2_engine.acknowledge_received_data(size, event.stream_id)
        await self.flush()

        stream = self.get_stream(event.stream_id)
        stream.process_data_received(event.data)

    async def process_stream_ended(self, event):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {event.stream_id} was ended."
        )
        stream = self.get_stream(event.stream_id)
        stream.process_eos()

    async def process_stream_reset(self, event):
        logger.info(
            f"[connection:{self.connection_id}] "
            f"Stream {event.stream_id} was reset. "
            f"Remote reset: {event.remote_reset}. "
            f"Error code: {event.error_code}. "
        )
        stream = self.get_stream(event.stream_id)
        if stream:
            stream.process_reset()
            self.forget_stream(event.stream_id)

    async def process_window_updated(self, event):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {event.stream_id} changed window. "
        )
        stream = self.get_stream(event.stream_id)
        if stream:
            stream.process_window_update()

    async def process_connection_terminated(self, event):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {event.stream_id} was closed. "
            f"Additional info: {event.additional_data}. "
            f"Error code: {event.error_code}. "
        )
        streams_ids = list(self.streams.keys())
        for stream_id in streams_ids:
            del self.streams[stream_id]

        self.transport.close()

    @raise_if_stream_closed
    async def acknowledge_received_data(self, stream_id, size):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {stream_id}: acked {size} bytes"
        )
        self.h2_engine.acknowledge_received_data(size, stream_id)
        await self.flush()

    async def flush(self):
        if not self.transport:
            error_message = "Cannot flush because connection is closed"
            logger.exception(
                f"[connection:{self.connection_id}] {error_message}"
            )
            raise RuntimeError(error_message)

        await self.transport.write(self.h2_engine.data_to_send())

    def create_stream(self, stream_id=None, *, serializer, cardinality, headers=None):
        stream_id = stream_id or self.h2_engine.get_next_available_stream_id()
        stream = self.stream_class(
            stream_id=stream_id,
            protocol=self,
            client_side=self.h2_engine.config.client_side,
            serializer=serializer,
            cardinality=cardinality,
            request_headers=headers,
        )
        self.streams[stream_id] = stream
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Created stream {stream.stream_id}"
        )
        diagnostics_logger.debug(
            f"[connection:{self.connection_id}] "
            f'Qty of active streams = {len(self.streams)}'
        )
        return stream

    def is_stream_alive(self, stream_id):
        return stream_id in self.streams

    def get_stream(self, stream_id):
        return self.streams.get(stream_id)

    def forget_stream(self, stream_id):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {stream_id} is gonna be forgotten"
        )
        self.streams.pop(stream_id, None)
        logger.debug(
            f"[connection:{self.connection_id}] "
            f'Active streams are {len(self.streams)}'
        )

    async def reset_stream(self, stream_id):
        logger.info(
            f"[connection:{self.connection_id}] "
            f"Stream {stream_id} is gonna be reset"
        )
        try:
            self.h2_engine.reset_stream(stream_id)
            await self.flush()
        except h2.exceptions.StreamClosedError:
            logger.warning(
                f"[connection:{self.connection_id}] "
                f"Stream {stream_id} is closed already. Cannot reset it"
            )

    @raise_if_stream_closed
    async def send_eos(self, stream_id):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {stream_id} is sending EOS"
        )
        self.h2_engine.end_stream(stream_id)
        await self.flush()

    @raise_if_stream_closed
    async def send_headers(self, stream_id, headers, end_stream=False):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {stream_id} is sending headers {headers}, "
            f"end_stream = {end_stream}"
        )
        headers = [(name, str(value)) for name, value in (headers or [])]
        self.h2_engine.send_headers(stream_id, headers, end_stream=end_stream)
        await self.flush()

    @raise_if_stream_closed
    async def send_data(self, stream_id, data, end_stream=False):
        logger.debug(
            f"[connection:{self.connection_id}] "
            f"Stream {stream_id}: sending data {data}, "
            f"end_stream = {end_stream}"
        )

        while data:
            chunk_size = self._get_writing_chunk_size(stream_id)
            if not chunk_size:
                logger.warning("[connection:{self.connection_id}] Writing window is zero. Other side is not reading?")
                stream = self.get_stream(stream_id)
                await stream.wait_window_next_update()
                continue

            sending_data, data = data[:chunk_size], data[chunk_size:]
            self.h2_engine.send_data(stream_id, sending_data)
            await self.flush()

    async def wait_stream_end_and_forget_it(self, stream_id, timeout=10):
        try:
            async with with_timeout(timeout):
                stream = self.get_stream(stream_id)
                if stream:
                    await stream.wait_stream_ended()
        except:
            logger.exception(
                f"[connection:{self.connection_id}] "
                f"Stream {stream_id} was not ended after "
                f"{timeout} seconds."
            )
        finally:
            self.forget_stream(stream_id)

    def _get_writing_chunk_size(self, stream_id):
        frame_size = self.h2_engine.max_outbound_frame_size
        window = self.h2_engine.local_flow_control_window(stream_id)

        result = min(frame_size - 1, window)
        if result <= 0:
            logger.warning(
                f"[connection:{self.connection_id}] "
                f"Too small values of window and frame_size: "
                f"frame_size={frame_size}, window={window}."
            )

        return result

    def _ignore_if_stream_closed(self, func):
        @functools.wraps(func)
        async def _func(event):
            stream = self.get_stream(event.stream_id)
            if not stream:
                logger.debug(
                    f"[connection:{self.connection_id}] "
                    f"Got event {event.__class__} for closed stream "
                    f"{event.stream_id}"
                )
                return

            await func(event)

        return _func

    def _generate_connection_id(self):
        id_seq = getattr(self.__class__, "_ID_SEQ", 0)
        next_id = id_seq + 1
        if next_id > 10 ** 7:
            next_id = 1
        setattr(self.__class__, "_ID_SEQ", next_id)
        return next_id


class ClientProtocol(Protocol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.h2_events_processors.update({
            h2.events.ResponseReceived: self._ignore_if_stream_closed(
                self.process_response_received
            ),
            h2.events.TrailersReceived: self._ignore_if_stream_closed(
                self.process_trailers_received
            ),
        })

    async def process_trailers_received(self, event):
        stream = self.get_stream(event.stream_id)
        stream.process_trailing_headers_received(event.headers)

    async def process_response_received(self, event):
        stream = self.get_stream(event.stream_id)
        stream.process_reply_headers_received(event.headers)


class ServerProtocol(Protocol):
    def __init__(self, request_processor, connections, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.request_processor = request_processor
        self.connections = connections

        self.h2_events_processors.update({
            h2.events.RequestReceived: self.process_request_received,
        })

    async def process_request_received(self, event):
        await self.request_processor(self, event)

    def connection_lost(self, *args, **kwargs):
        super().connection_lost(*args, **kwargs)

        if self in self.connections:
            logger.debug(f"[connection:{self.connection_id}] Removing from registry.")
            self.connections.remove(self)

    def connection_made(self, *args, **kwargs):
        super().connection_made(*args, **kwargs)

        logger.debug(f"[connection:{self.connection_id}] Adding to registry.")
        self.connections.add(self)


class TransportWrapper:
    def __init__(self, sock, loop=None):
        self.sock = sock
        self.loop = loop or asyncio.get_event_loop()

        self._write_ready = Event(loop=self.loop)
        self._write_ready.set()

    async def write(self, data):
        await self._write_ready.wait()
        self.sock.write(data)

    def pause(self):
        logger.debug(f"Connection's writing is paused")
        self._write_ready.clear()

    def resume(self):
        logger.debug(f"Connection's writing is resumed")
        self._write_ready.set()

    def close(self):
        self.sock.close()

    def closed(self):
        return self.sock.is_closing()


def _set_transport_socket_nodelay(transport):
    sock = transport.get_extra_info('socket')
    if sock is not None:
        _set_nodelay(sock)


def _set_nodelay(sock):
    if not hasattr(socket, 'TCP_NODELAY'):
        return

    sock_type_mask = 0xf if hasattr(socket, 'SOCK_NONBLOCK') else 0xffffffff

    if (
            sock.family in (socket.AF_INET, socket.AF_INET6)
            and sock.type & sock_type_mask == socket.SOCK_STREAM
            and sock.proto == socket.IPPROTO_TCP
    ):
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
