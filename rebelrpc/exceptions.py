class RebelRPCError(Exception):
    pass


class StreamTerminatedError(RebelRPCError):
    pass


class BadStatusError(RebelRPCError):
    def __init__(self, http_status, message=None):
        error_message = (
            f"Bad status: "
            f"http_status: '{http_status}', "
            f"message: {message}."
        )

        self.http_status = http_status
        self.message = message

        super().__init__(error_message)


class StreamTimeoutError(RebelRPCError):
    pass


class ClientTimeoutError(RebelRPCError):
    pass


class CardinalityError(RebelRPCError):
    pass


class NoHeadersError(RebelRPCError):
    pass
