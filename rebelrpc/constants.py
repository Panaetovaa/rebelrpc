import enum
import collections


DEFAULT_ENCODING = 'utf-8'
DEFAULT_STREAM_TIMEOUT = 30


_Cardinality = collections.namedtuple(
    '_Cardinality', 'client_streaming, server_streaming',
)


class Cardinality(enum.Enum):
    UNARY_UNARY = _Cardinality(False, False)
    UNARY_STREAM = _Cardinality(False, True)
    STREAM_UNARY = _Cardinality(True, False)
    STREAM_STREAM = _Cardinality(True, True)

    @classmethod
    def get(cls, client_streaming, server_streaming):
        if client_streaming:
            if server_streaming:
                return cls.STREAM_STREAM
            else:
                return cls.STREAM_UNARY
        else:
            if server_streaming:
                return cls.UNARY_STREAM
            else:
                return cls.UNARY_UNARY


Handler = collections.namedtuple(
    'Handler', 'func, cardinality, request_message_type, reply_message_type, error_message_type',
)


MIN_WRITE_CHUNK = 128
