import asyncio
import functools
import logging
import time
import weakref

from async_timeout import timeout as with_timeout

from rebelrpc.protocol import ServerProtocol
from rebelrpc.loggers import diagnostics_logger
from rebelrpc.serializer import Serializer


logger = logging.getLogger(__name__)


class Server:
    def __init__(self, handler, spec, *, loop=None, middlewares=None, timeouts=None, timeout=None):
        self.handler = handler
        self.spec = spec
        self.middlewares = middlewares or []
        self.loop = loop or asyncio.get_event_loop()

        self._connections = weakref.WeakSet()
        self._tcp_server = None
        self.timeout = timeout
        self.timeouts = timeouts

        self._connections_qty_logger = asyncio.ensure_future(
            self._log_connections_qty(), loop=self.loop
        )

    def get_connections(self):
        return self._connections

    async def _log_connections_qty(self):
        while True:
            try:
                diagnostics_logger.info(
                    f"Connections: {[conn.connection_id for conn in self._connections]}, "
                    f"Streams: {[len(conn.streams) for conn in self._connections]}."
                )
            except:
                logger.exception("Cannot print connections info")
            finally:
                await asyncio.sleep(60)

    def _create_protocol(self):
        h2_config = {
            'client_side': False,
            'header_encoding': 'utf-8',
        }
        protocol = ServerProtocol(
            RequestProcessor(self), self._connections,
            h2_config, loop=self.loop,
        )
        return protocol

    async def start(self, host='127.0.0.1', port=8000, *, backlog=100):
        """ Coroutine to start the server.

        :param host: can be a string, containing IPv4/v6 address or domain name.
            If host is None, server will be bound to all available interfaces.

        :param port: port number.

        :param backlog: is the maximum number of queued connections passed to
            listen().
        """
        if self._tcp_server is not None:
            raise RuntimeError('Server is already started')

        host = self.host = str(host)
        port = self.port = int(port)

        self._tcp_server = await self.loop.create_server(
            self._create_protocol, host, port, backlog=backlog,
        )

    def close(self):
        if self._tcp_server:
            self._tcp_server.close()

        for conn in self._connections:
            conn.close()

        self._connections_qty_logger.cancel()

    async def wait_closed(self):
        if self._tcp_server:
            await self._tcp_server.wait_closed()


class RequestProcessor:
    def __init__(self, server):
        self.server = server

    async def __call__(self, protocol, event):
        existing_stream = protocol.get_stream(event.stream_id)
        if existing_stream:
            raise RuntimeError(f"Stream {event.stream_id} is processing already")

        requested_path = dict(event.headers).get(':path')
        logger.info(
            f"[connection:{protocol.connection_id}] "
            f"Request received: path={requested_path}, "
            f"stream={event.stream_id}."
        )

        method_spec = self._get_spec(requested_path)
        if not method_spec:
            logger.warning(
                f"[connection:{protocol.connection_id}] "
                f"Method is not implemented: path={requested_path}, "
                f"stream={event.stream_id}."
            )
            await protocol.send_headers(event.stream_id, {":status": 404}, end_stream=True)
            return

        serializer = Serializer(
            cardinality=method_spec['cardinality'],
            request_type=method_spec.get('request_type'),
            reply_type=method_spec.get('reply_type'),
            error_type=method_spec.get('error_type'),
        )

        stream = protocol.create_stream(
            stream_id=event.stream_id,
            headers=event.headers,
            cardinality=method_spec['cardinality'],
            serializer=serializer,
        )

        t_started = time.time()
        try:
            await self._process_request_received(protocol, stream, method_spec['name'])
        finally:
            logger.info(
                f"[connection:{protocol.connection_id}] "
                f"Request done: path={stream.requested_path}, "
                f"status={stream.http_status}, "
                f"stream={stream.stream_id}, "
                f"duration={time.time() - t_started}"
            )
            asyncio.ensure_future(
                protocol.wait_stream_end_and_forget_it(stream.stream_id)
            )

    def _get_spec(self, path):
        target_method_name, target_spec = None, None

        for method_name, spec in self.server.spec.items():
            if spec.get('path') == path:
                target_method_name = method_name
                target_spec = spec
                break

        if not target_method_name:
            for method_name, spec in self.server.spec.items():
                if method_name == path.strip('/'):
                    target_method_name = method_name
                    target_spec = spec
                    break

        if target_method_name:
            return dict(target_spec, name=method_name)
        else:
            return None

    def _get_timeout(self, method_name):
        return (self.server.timeouts or {}).get(method_name, self.server.timeout or None)

    def _get_handler(self, method_name):
        return getattr(self.server.handler(), method_name)

    async def _process_request_received(self, protocol, stream, method_name):
        handler = self._get_handler(method_name)
        if not handler:
            await protocol.send_headers(stream.stream_id, {":status": 404}, end_stream=True)
            return

        func = wrap_handler_with_middlewares(
            handler, self.server.middlewares,
        )
        timeout = self._get_timeout(method_name)
        try:
            async with with_timeout(timeout):
                task = asyncio.Task(func(stream))
                stream.subscribe_on_reset(lambda: task.cancel())
                await task

        except asyncio.CancelledError:
            logger.warning(
                f"[connection:{protocol.connection_id}] "
                f"Request to {stream.requested_path} cancelled because stream was reset by client"
            )

        except:
            logger.exception(
                f"[connection:{protocol.connection_id}] "
                f"Request to {stream.requested_path} failed because of server error"
            )
            if protocol.is_stream_alive(stream.stream_id):
                await protocol.reset_stream(stream.stream_id)
                protocol.forget_stream(stream.stream_id)

        else:
            logger.debug(
                f"[connection:{protocol.connection_id}] "
                f"Request to stream {stream.stream_id} is finished."
            )
            if stream.is_alive():
                await stream.send_headers()
                await stream.send_eos()


def wrap_handler_with_middlewares(real_handler, middlewares):
    handler = real_handler
    for middleware in middlewares[::-1]:
        handler = functools.partial(middleware, continuation=handler)

    return handler
