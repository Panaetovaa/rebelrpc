import json
import os.path

from rebelrpc import constants
from rebelrpc.serializer import MessageType


def parse_spec_file(path):
    with open(path, 'r') as f:
        content = f.read()

    methods_options = json.loads(content)

    base_dir = os.path.dirname(path)
    return build_spec(methods_options, base_dir)


def build_spec(methods_options, base_dir=''):
    spec = {}

    make_full_path = lambda path: os.path.join(base_dir or '', path) if path else None
    for method, options in methods_options.items():
        spec[method] = {}

        spec[method]['cardinality'] = constants.Cardinality.get(
            client_streaming=options.get('client_streaming', False),
            server_streaming=options.get('server_streaming', False),
        )
        spec[method]['path'] = options.get('path', method)
        spec[method]['request_type'] = MessageType(
            make_full_path(options.get('request_type')), base_dir,
        )
        spec[method]['reply_type'] = MessageType(
            make_full_path(options.get('reply_type')), base_dir,
        )
        spec[method]['error_type'] = MessageType(
            make_full_path(options.get('error_type')), base_dir,
        )

    return spec
