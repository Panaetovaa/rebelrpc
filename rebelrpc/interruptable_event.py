import asyncio


class InterruptableEvent:
    def __init__(self):
        self.futures = []
        self.exception = None
        self.got_result = False

    def set(self):
        while self.futures:
            future = self.futures.pop()
            if not future.done():
                future.set_result(None)

        self.got_result = True

    def clear(self):
        self.exception = None
        self.got_result = False

    def interrupt(self, exception):
        while self.futures:
            self.futures.pop().set_exception(exception)

        self.exception = exception

    def wait(self):
        future = asyncio.Future()
        if self.exception:
            future.set_exception(self.exception)

        elif self.got_result:
            future.set_result(None)

        if not future.done():
            self.futures.append(future)

        return future
