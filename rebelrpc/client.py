import asyncio
import datetime
import logging

from async_timeout import timeout as with_timeout

from async_generator import asynccontextmanager
from async_generator import async_generator
from async_generator import yield_from_

from rebelrpc import constants
from rebelrpc.exceptions import BadStatusError
from rebelrpc.exceptions import CardinalityError
from rebelrpc.exceptions import ClientTimeoutError
from rebelrpc.protocol import ClientProtocol
from rebelrpc.serializer import Serializer


logger = logging.getLogger(__name__)


class Client:
    """ Represents transport client from client to server. Channel can be reusable
        by multiple clients.

        .. code-block:: python
            client = Client({"ping": {"cardinality": "stream_stream"}}, port=port)
    """
    def __init__(self, spec, host='127.0.0.1', port=50051, *, loop=None, middlewares=None):
        """
            :param host: host of server
            :param port: port of server
            :param middlewares: list of middlewares used on client side.
            :param loop: asyncio-compatible event loop

            As an example of client middleware you can check function
            `_print_request_duration_middleware` declared bellow in this module.
        """
        self.host = str(host)
        self.port = int(port)
        self.loop = loop or asyncio.get_event_loop()

        self.protocol = None
        self.middlewares = middlewares or []
        self.middlewares.insert(0, _print_request_duration_middleware)

        self._set_methods(spec)

    def _set_methods(self, spec):
        classes = {
            constants.Cardinality.UNARY_UNARY: UnaryUnaryMethod,
            constants.Cardinality.UNARY_STREAM: UnaryStreamMethod,
            constants.Cardinality.STREAM_UNARY: StreamUnaryMethod,
            constants.Cardinality.STREAM_STREAM: StreamStreamMethod,
        }
        for method_name, method_spec in spec.items():
            method_class = classes[method_spec['cardinality']]
            serializer = Serializer(
                cardinality=method_spec['cardinality'],
                request_type=method_spec.get('request_type'),
                reply_type=method_spec.get('reply_type'),
                error_type=method_spec.get('error_type'),
            )
            setattr(self, method_name, method_class(self, method_spec['path'], serializer))

    def _create_protocol(self):
        logger.info(f"Connecting {self.host}:{self.port}")
        h2_config = {
            'client_side': True,
            'header_encoding': 'utf-8',
        }
        protocol = ClientProtocol(h2_config, loop=self.loop)
        return protocol

    async def connect(self):
        if not self.protocol or self.protocol.closed():
            _, self.protocol = await self.loop.create_connection(
                self._create_protocol, self.host, self.port
            )
        else:
            logger.debug(f"Client {self.host}:{self.port} is using existing connection.")

        return self.protocol

    def closed(self):
        return not self.protocol or self.protocol.closed()

    def close(self):
        if not self.closed():
            self.protocol.close()


class ClientReply:
    """ Instance of this class will be result of client call.
    """
    def __init__(self, messages, reply_headers):
        self._messages = messages
        self.reply_headers = reply_headers

    @property
    def http_status(self):
        return int(self.get_header(':status'))

    def get_header(self, wanted_name):
        return self.reply_headers.get(wanted_name)

    @property
    def messages(self):
        raw_messages = (
            self._messages
            if isinstance(self._messages, (list, tuple)) else
            [self._messages]
        )
        return [message for message in raw_messages if message is not None]

    @property
    def message(self):
        return self.messages[0] if self._messages else None

    def __repr__(self):
        headers_repr = "\n".join(
            f"{header}: {header_value}"
            for header, header_value in self.reply_headers.items()
        )

        messages_repr = "\n\n".join([
            repr(message) for message in self.messages
        ])
        return f"{headers_repr}\n\n{messages_repr}"


class Method:
    """ Represents method of RPC server. Instances of this class are creating
        automatically during code generation.
    """
    cardinality = None

    def __init__(self, client, path, serializer):
        self.client = client
        self.path = path
        self.serializer = serializer

    @asynccontextmanager
    @async_generator
    async def connect(self, *, timeout=None, headers=None):
        """ Context manager. It creates stream in client of this method and
            destroys it when context is over.

            .. code-block:: python
                async with self.connect(timeout=timeout, headers=headers) as stream:
                    await stream.send_message(message)

            :param integer timeout: timeout of client request. If request is
            not finished after timeout exception rebelrpc.exceptions.ClientTimeoutError
            will be raised.
            :param dict headers: headers of client request.

        """
        logger.info(f"Calling method '{self.path}' on {self.client.host}:{self.client.port}.")
        protocol = await self.client.connect()

        headers = dict(headers or {})
        headers.update({
            ':path': self.path,
            ':authority': f'{self.client.host}:{self.client.port}',
        })
        stream = protocol.create_stream(
            serializer=self.serializer,
            cardinality=self.cardinality,
            headers=headers,
        )

        try:
            await yield_from_(wrap_request_with_middlewares(
                self._yield_request_stream, self.client.middlewares, timeout,
            )(stream))
        except BadStatusError as e:
            logger.debug(f"Request to {self.path} returned bad status. {e}")
            raise

        except:
            if stream.is_alive():
                await stream.reset()
            raise

        finally:
            protocol.forget_stream(stream.stream_id)

    async def _yield_request_stream(self, stream):
        await stream.send_headers(stream.request_headers)
        yield stream
        if stream.is_alive():
            await stream.send_eos()
            await stream.wait_stream_ended()

    async def __call__(self, messages=None, *, timeout=None, headers=None,
                       raise_on_errors=True):
        """ Calls RPC method.

            :param list/dict messages: message or list of messages which
            will be send to server

            :param timeout: timeout of call

            :param dict headers: headers of request

            :param bool raise_on_errors: if it is True, then if server
            replies with http status >= 400, exception rebelrpc.exceptions.BadStatusError
            will be raised
        """
        if self.cardinality.value.client_streaming:
            messages = messages or []
        else:
            messages = messages if messages is not None else []

        if not isinstance(messages, (list, tuple)):
            messages = [messages]

        if not self.cardinality.value.client_streaming and len(messages) > 1:
            raise CardinalityError(
                "Client is not in streaming mode. It cannot send more than 1 message"
            )

        async with self.connect(timeout=timeout, headers=headers) as stream:
            for message in messages:
                await stream.send_message(message)

            await stream.send_eos()

            reply_messages = await self._receive_reply_messages(stream)
            await stream.wait_stream_ended()

            reply_wrapper = ClientReply(reply_messages, stream.reply_headers)
            if raise_on_errors and (
                    not reply_wrapper.http_status or reply_wrapper.http_status >= 400
            ):
                raise BadStatusError(
                    message=reply_wrapper.message, http_status=reply_wrapper.http_status,
                )

            return reply_wrapper

    async def _receive_reply_messages(self, stream):
        reply_messages = [
            message async for message in stream.receive_all_messages()
        ]

        if self.cardinality.value.server_streaming:
            return reply_messages
        elif reply_messages:
            return reply_messages[0]
        else:
            return None


class UnaryUnaryMethod(Method):
    cardinality = constants.Cardinality.UNARY_UNARY


class UnaryStreamMethod(Method):
    cardinality = constants.Cardinality.UNARY_STREAM


class StreamUnaryMethod(Method):
    cardinality = constants.Cardinality.STREAM_UNARY


class StreamStreamMethod(Method):
    cardinality = constants.Cardinality.STREAM_STREAM


def wrap_request_with_middlewares(handler, middlewares, timeout):
    continuation = _make_continuation(handler)
    for middleware in middlewares[::-1]:
        handler = _wrap_handler_with_middleware(middleware, continuation)
        continuation = _make_continuation(handler, async_generator)

    @async_generator
    async def _wrapper(stream):
        try:
            async with with_timeout(timeout or constants.DEFAULT_STREAM_TIMEOUT):
                await handler(stream)
        except asyncio.TimeoutError:
            logger.warning("Request was cancelled by timeout")
            raise ClientTimeoutError() from None

    return _wrapper


def _wrap_handler_with_middleware(middleware, continuation):
    def _wrapper(stream):
        return yield_from_(async_generator(middleware)(stream, continuation))
    return _wrapper


def _make_continuation(handler, wrapper=None):
    def _continuation(stream):
        nonlocal handler
        if wrapper:
            handler = wrapper(handler)

        return yield_from_(handler(stream))
    return _continuation


async def _print_request_duration_middleware(stream, continuation):
    t_begin = datetime.datetime.now()
    await continuation(stream)
    duration = datetime.datetime.now() - t_begin
    logger.debug(f"Duration of call to {stream.requested_path} is {duration}")
