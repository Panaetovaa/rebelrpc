import logging

from asyncio import Lock

from async_timeout import timeout as with_timeout

from rebelrpc.interruptable_event import InterruptableEvent


logger = logging.getLogger(__name__)


class BinaryBuffer:
    def __init__(self):
        self.data = b""
        self._new_data_event = InterruptableEvent()
        self._eof_event = InterruptableEvent()
        self.eof = False

    def set_eof(self):
        self.eof = True
        self._new_data_event.set()
        self._eof_event.set()

    async def wait_eof(self):
        await self._eof_event.wait()

    def interrupt(self, exception):
        self._new_data_event.interrupt(exception)
        self._eof_event.interrupt(exception)

    def write(self, data):
        self.data += data
        self._new_data_event.set()

    async def read(self, size=None, timeout=None):
        async with Lock():
            async with with_timeout(timeout):
                return await self._read(size)

    async def _read(self, size=None):
        size = size or len(self.data)

        while len(self.data) < size and not self.eof:
            self._new_data_event.clear()
            await self._new_data_event.wait()

        chunk, self.data = self.data[:size], self.data[size:]
        return chunk
