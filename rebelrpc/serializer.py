import json
import struct

import jsonref
import jsonschema

from rebelrpc import constants


class BaseSerializer:
    def __init__(self, *, cardinality, request_type, reply_type, error_type):
        self.cardinality = cardinality
        self.request_type = request_type
        self.reply_type = reply_type
        self.error_type = error_type

    def serialize_error_message(self, message):
        raise NotImplementedError

    def serialize_request_message(self, message):
        raise NotImplementedError

    def serialize_reply_message(self, message):
        raise NotImplementedError

    async def pull_request_message(self, source):
        raise NotImplementedError

    async def pull_reply_message(self, source):
        raise NotImplementedError

    async def pull_error_message(self, source):
        raise NotImplementedError


class Serializer(BaseSerializer):
    META_LEN = 4

    def serialize_error_message(self, message):
        return self._serialize_unary_message(message, self.error_type)

    def serialize_request_message(self, message):
        if self.cardinality.value.client_streaming:
            return self._serialize_streaming_message(message, self.request_type)
        else:
            return self._serialize_unary_message(message, self.request_type)

    def serialize_reply_message(self, message):
        if self.cardinality.value.server_streaming:
            return self._serialize_streaming_message(message, self.reply_type)
        else:
            return self._serialize_unary_message(message, self.reply_type)

    async def pull_request_message(self, source):
        if self.cardinality.value.client_streaming:
            return await self._pull_streaming_message(source, self.request_type)
        else:
            return await self._pull_unary_message(source, self.request_type)

    async def pull_reply_message(self, source):
        if self.cardinality.value.server_streaming:
            return await self._pull_streaming_message(source, self.reply_type)
        else:
            return await self._pull_unary_message(source, self.reply_type)

    async def pull_error_message(self, source):
        return await self._pull_unary_message(source, self.error_type)

    async def _pull_unary_message(self, source, message_type):
        await source.wait_eof()

        bin_data = await source.read()
        if not bin_data:
            return

        return message_type.from_string(bin_data)

    async def _pull_streaming_message(self, source, message_type):
        meta = await source.read(self.META_LEN)
        if not meta:
            return

        data_len = struct.unpack('>I', meta)[0]
        data_bin = await source.read(data_len)
        if len(data_bin) != data_len:
            raise RuntimeError(
                f"Message length = {len(data_bin)}."
                f"It must be {data_len}."
            )

        return message_type.from_string(data_bin)

    def _serialize_streaming_message(self, message, message_type):
        data_bin = message_type.to_string(message)
        return struct.pack('>I', len(data_bin)) + data_bin

    def _serialize_unary_message(self, message, message_type):
        return message_type.to_string(message)


class MessageType:
    def __init__(self, ref, base_uri=None):
        self.ref = ref
        self.base_uri = base_uri
        self.schema = self.build_schema()

    def __repr__(self):
        return f"$ref: {self.ref}, base_uri: {self.base_uri}"

    def to_string(self, data):
        self.validate(data)
        return json.dumps(data).encode(constants.DEFAULT_ENCODING)

    def from_string(self, data_bin):
        message = json.loads(data_bin)

        self.validate(message)
        message = _feed_steroids(message)
        return message

    def build_schema(self):
        if self.ref:
            return jsonref.loads(
                json.dumps({'$ref': f'file:{self.ref}'}),
                base_uri=self.base_uri,
            )

        else:
            return {}

    def validate(self, message):
        return jsonschema.validate(message, self.schema)


class Dict(dict):
    """
        >>> obj = Dict(a=10, b=20)
        >>> print obj.a
        >>> 10
        >>> print obj['a']
        >>> 10
        >>> obj.a = 11
        >>> print obj['a']
        >>> 11
    """

    def __init__(self, *args, **kwargs):
        super(Dict, self).__init__(*args, **kwargs)
        for k, v in self.items():
            self[k] = _feed_steroids(v)

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)


def _feed_steroids(val):
    if isinstance(val, dict):
        return Dict(val)

    if isinstance(val, list):
        return [_feed_steroids(item) for item in val]

    return val
