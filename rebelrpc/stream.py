import asyncio
import collections
import logging

from asyncio import Lock

from rebelrpc.exceptions import BadStatusError
from rebelrpc.exceptions import CardinalityError
from rebelrpc.exceptions import StreamTerminatedError
from rebelrpc.interruptable_event import InterruptableEvent
from rebelrpc.buffer import BinaryBuffer


logger = logging.getLogger(__name__)


class Stream:
    def __init__(
            self, stream_id, protocol, client_side,
            *,
            serializer=None,
            cardinality=None,
            request_headers=None,
            loop=None,
    ):
        self.loop = loop or asyncio.get_event_loop()
        self.stream_id = stream_id
        self.protocol = protocol
        self.client_side = client_side

        self.serializer = serializer
        self.cardinality = cardinality

        self.request_headers = dict(request_headers or {})
        self.reply_headers = {}

        self._messages_sent = False
        self._source = BinaryBuffer()
        self._lookahead_buffer = collections.deque()
        self._headers_sending_lock = Lock()
        self._headers_sent = False
        self._stream_ended_event = InterruptableEvent()
        self._reply_headers_received_event = InterruptableEvent()
        self._window_updated_event = InterruptableEvent()
        self._reset_subscribers = []
        self._eos_sent = False

    @property
    def standard_headers(self):
        if self.client_side:
            return [
                (':method', _set_default('POST')),
                (':scheme', _set_default('http')),
                (':path', _raise_required_error),
                (':authority', _skip),
            ]
        else:
            return [
                (':status', _set_default('200')),
            ]

    async def send_error(self, http_code, message):
        await self.send_headers({':status': http_code})
        bin_data = self.serializer.serialize_error_message(message)
        await self.protocol.send_data(self.stream_id, bin_data)

    async def send_eos(self):
        if not self._eos_sent:
            await self.protocol.send_eos(self.stream_id)
            self._eos_sent = True

    def subscribe_on_reset(self, cb):
        self._reset_subscribers.append(cb)

    def headers_were_sent(self):
        return self._headers_sent

    @property
    def requested_path(self):
        return self.get_request_header(':path')

    @property
    def http_status(self):
        raw_status = self.get_reply_header(':status')
        return int(raw_status) if raw_status else None

    def update_request_headers(self, headers):
        self.request_headers.update(headers)

    def get_request_header(self, name, default=''):
        return self._get_header(self.request_headers, name, default)

    def get_reply_header(self, name, default=''):
        return self._get_header(self.reply_headers, name, default)

    def _get_header(self, headers, name, default):
        normalized_headers = {
            name.upper(): value for name, value in headers.items()
        }
        return normalized_headers.get(name.upper(), default)

    def is_alive(self):
        return self.protocol.is_stream_alive(self.stream_id)

    def wait_stream_ended(self):
        return self._stream_ended_event.wait()

    async def lookahead_messages(self, qty):
        while len(self._lookahead_buffer) < qty:
            message = await self._receive_message()
            if message:
                self._lookahead_buffer.append(message)
            else:
                # There are no messages any more. Sorry, man :(
                break

        return self._lookahead_buffer[:qty]

    async def receive_message(self):
        if self._lookahead_buffer:
            message = self.lookahead_buffer.popleft()
            return message

        message = await self._receive_message()
        return message

    async def receive_all_messages(self):
        while True:
            message = await self.receive_message()
            if not message:
                break
            yield message

    async def send_headers(self, headers=None):
        headers = headers or {}
        async with self._headers_sending_lock:
            if not self._headers_sent:
                headers = self.build_initial_headers(headers)

            if not headers:
                return

            if isinstance(headers, dict):
                headers = list(headers.items())

            logger.debug(
                f"[connection:{self.protocol.connection_id}] "
                f"Stream {self.stream_id} is sending headers {headers}"
            )
            await self.protocol.send_headers(
                self.stream_id, headers
            )
            if self.client_side:
                self.request_headers.update(headers)
            else:
                self.reply_headers.update(headers)

            self._headers_sent = True

    def message_sent(self):
        return self._messages_sent

    async def send_message(self, message):
        if not self._headers_sent:
            await self.send_headers()

        if self.client_side:
            if not self.cardinality.value.client_streaming:
                if self._messages_sent:
                    raise CardinalityError("Cannot send more than one message")

            bin_data = self.serializer.serialize_request_message(message)
        else:
            if self._messages_sent and not self.cardinality.value.server_streaming:
                raise CardinalityError("Cannot send more than one message")

            bin_data = self.serializer.serialize_reply_message(message)

        await self.protocol.send_data(self.stream_id, bin_data)

        if self.client_side and not self.cardinality.value.client_streaming:
            await self.send_eos()

        self._messages_sent = True

    async def reset(self):
        await self.protocol.reset_stream(self.stream_id)

    def process_reply_headers_received(self, headers):
        logger.debug(
            f"[connection:{self.protocol.connection_id}] "
            f"Stream {self.stream_id} received headers {headers}"
        )
        self._merge_reply_headers(headers)
        self._reply_headers_received_event.set()

    def process_trailing_headers_received(self, headers):
        logger.debug(
            f"[connection:{self.protocol.connection_id}] "
            f"Stream {self.stream_id} received trailers {headers}"
        )
        self._merge_reply_headers(headers)

    def process_data_received(self, data):
        self._source.write(data)

    def process_window_update(self):
        self._window_updated_event.set()

    def process_eos(self):
        self._source.set_eof()
        self._stream_ended_event.set()

    def process_reset(self):
        self._stream_ended_event.set()

        exception = StreamTerminatedError(f"Stream {self.stream_id} was reset")

        self._source.interrupt(exception)
        self._reply_headers_received_event.interrupt(exception)
        self._window_updated_event.interrupt(exception)

        for cb in self._reset_subscribers:
            self._call_reset_subscriber(cb)

    async def wait_reply_received(self, raise_on_errors=True):
        await self._reply_headers_received_event.wait()
        if self.http_status >= 400 and raise_on_errors:
            message = await self.serializer.pull_error_message(self._source)
            raise BadStatusError(
                message=message, http_status=self.http_status,
            )

    def wait_window_next_update(self):
        self._window_updated_event.clear()
        return self._window_updated_event.wait()

    async def _receive_message(self):
        if self.client_side:
            await self.wait_reply_received()

            if self.http_status >= 400:
                return await self.serializer.pull_error_message(self._source)
            else:
                return await self.serializer.pull_reply_message(self._source)
        else:
            return await self.serializer.pull_request_message(self._source)

    def build_initial_headers(self, headers):
        headers = dict(headers)
        result = []
        for header_name, action in self.standard_headers:
            if header_name in headers:
                result.append((header_name, headers.pop(header_name)))
            else:
                action(result, header_name)

        custom_headers = [
            (header_name, header_value)
            for header_name, header_value in headers.items()
            if header_name in headers
        ]
        result.extend(custom_headers)
        return result

    def _call_reset_subscriber(self, cb):
        try:
            cb()
        except:
            logger.warning(f"Cannot call {cb} on reset", exc_info=True)

    def _merge_reply_headers(self, new_headers):
        if not new_headers:
            return

        self.reply_headers.update(new_headers)


def _set_default(default):
    def _action(headers, header_name):
        headers.append((header_name, default))

    return _action


def _skip(*_, **__):
    pass


def _raise_required_error(headers, header_name):
    raise RuntimeError(f'Header {header_name} is required')
