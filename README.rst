This project is a pure-Python rebelRPC async implementation, based on hyper-h2
project, **requires Python >= 3.6**.

Installation
~~~~~~~~~~~~

.. code-block:: shell

    $ pip3 install rebelrpc

Example
~~~~~~~

See ``demo`` directory for a full example of the ``dummy`` service.
