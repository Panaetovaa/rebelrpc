import random

import pytest

from rebelrpc.exceptions import BadStatusError
from rebelrpc.exceptions import StreamTerminatedError

from tests.services.dummy_service import BaseHandler
from tests.services.dummy_service import get_dummy_spec


@pytest.mark.asyncio
@pytest.mark.flows
async def test_server_unexpected_fail(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            raise RuntimeError("Shit happened")

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        await client.unary_unary_method({'int_parameter': 100})
    except StreamTerminatedError:
        pass
    else:
        pytest.fail("BadStatusError was not raised")


@pytest.mark.asyncio
@pytest.mark.flows
async def test_server_unexpected_fail_long_call(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.send_error(404, {'code': 'TEST_CODE'})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    async with client.unary_unary_method.connect() as stream:
        await stream.send_message({'int_parameter': 1})

        try:
            await stream.wait_reply_received()
        except BadStatusError as e:
            assert e.message.code == 'TEST_CODE'
            assert e.http_status == 404
        else:
            pytest.fail("BadStatusError was not raised")


@pytest.mark.asyncio
@pytest.mark.flows
async def test_server_regular_fail(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.send_error(404, {'code': 'TEST_CODE'})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        await client.unary_unary_method({'int_parameter': 100})
    except BadStatusError as e:
        assert e.message.code == 'TEST_CODE'
        assert e.http_status == 404
    else:
        pytest.fail("BadStatusError was not raised")


@pytest.mark.asyncio
@pytest.mark.flows
async def test_server_regular_fail_long_call(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.send_error(404, {'code': 'TEST_CODE'})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    async with client.unary_unary_method.connect() as stream:
        await stream.send_message({'int_parameter': 1})

        try:
            await stream.wait_reply_received()
        except BadStatusError as e:
            assert e.message.code == 'TEST_CODE'
            assert e.http_status == 404
        else:
            pytest.fail("BadStatusError was not raised")


@pytest.mark.asyncio
@pytest.mark.flows
async def test_unary_unary_success_flow(server_factory, client_factory):
    int_parameter = random.randint(0, 1000)

    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            message = await stream.receive_message()
            await stream.send_message({'int_parameter': message.int_parameter})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    reply = await client.unary_unary_method({'int_parameter': int_parameter})

    assert reply.http_status == 200
    assert reply.message.int_parameter == int_parameter


@pytest.mark.asyncio
@pytest.mark.flows
async def test_unary_unary_success_flow_long_call(server_factory, client_factory):
    int_parameter = random.randint(0, 1000)

    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            message = await stream.receive_message()
            await stream.send_message({'int_parameter': message.int_parameter})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    async with client.unary_unary_method.connect() as stream:
        await stream.send_message({'int_parameter': int_parameter})

        await stream.wait_reply_received()
        assert stream.http_status == 200

        assert (await stream.receive_message()).int_parameter == int_parameter
        assert not await stream.receive_message()


@pytest.mark.asyncio
@pytest.mark.flows
async def test_unary_stream_success_flow(server_factory, client_factory):
    int_parameter = random.randint(0, 1000)

    class Handler(BaseHandler):
        async def unary_stream_method(self, stream):
            message = await stream.receive_message()
            await stream.send_message({'int_parameter': message.int_parameter})
            await stream.send_message({'int_parameter': message.int_parameter + 1})
            await stream.send_message({'int_parameter': message.int_parameter + 2})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    reply = await client.unary_stream_method({'int_parameter': int_parameter})

    assert reply.http_status == 200
    assert reply.messages[0].int_parameter == int_parameter
    assert reply.messages[1].int_parameter == int_parameter + 1
    assert reply.messages[2].int_parameter == int_parameter + 2


@pytest.mark.asyncio
@pytest.mark.flows
async def test_unary_stream_success_flow_long_call(server_factory, client_factory):
    int_parameter = random.randint(0, 1000)

    class Handler(BaseHandler):
        async def unary_stream_method(self, stream):
            message = await stream.receive_message()
            await stream.send_message({'int_parameter': message.int_parameter})
            await stream.send_message({'int_parameter': message.int_parameter + 1})
            await stream.send_message({'int_parameter': message.int_parameter + 2})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    async with client.unary_stream_method.connect() as stream:
        await stream.send_message({'int_parameter': int_parameter})

        await stream.wait_reply_received()
        assert stream.http_status == 200

        assert (await stream.receive_message()).int_parameter == int_parameter
        assert (await stream.receive_message()).int_parameter == int_parameter + 1
        assert (await stream.receive_message()).int_parameter == int_parameter + 2
        assert not await stream.receive_message()


@pytest.mark.asyncio
@pytest.mark.flows
async def test_stream_stream_success_flow(server_factory, client_factory):
    int_parameter1 = random.randint(0, 1000)
    int_parameter2 = int_parameter1 + 1

    class Handler(BaseHandler):
        async def stream_stream_method(self, stream):
            message1 = await stream.receive_message()
            await stream.send_message({'int_parameter': message1.int_parameter})
            await stream.send_message({'int_parameter': message1.int_parameter + 1})

            message2 = await stream.receive_message()
            await stream.send_message({'int_parameter': message2.int_parameter + 10})
            await stream.send_message({'int_parameter': message2.int_parameter + 11})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    reply = await client.stream_stream_method([
        {'int_parameter': int_parameter1},
        {'int_parameter': int_parameter2},
    ])

    assert reply.http_status == 200
    assert reply.messages[0].int_parameter == int_parameter1
    assert reply.messages[1].int_parameter == int_parameter1 + 1
    assert reply.messages[2].int_parameter == int_parameter2 + 10
    assert reply.messages[3].int_parameter == int_parameter2 + 11


@pytest.mark.asyncio
@pytest.mark.flows
async def test_stream_stream_success_flow_long_call(server_factory, client_factory):
    int_parameter1 = random.randint(0, 1000)
    int_parameter2 = int_parameter1 + 1

    class Handler(BaseHandler):
        async def stream_stream_method(self, stream):
            message1 = await stream.receive_message()

            await stream.send_message({'int_parameter': message1.int_parameter})
            await stream.send_message({'int_parameter': message1.int_parameter + 1})

            message2 = await stream.receive_message()
            await stream.send_message({'int_parameter': message2.int_parameter + 10})
            await stream.send_message({'int_parameter': message2.int_parameter + 11})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    async with client.stream_stream_method.connect() as stream:
        await stream.send_message({'int_parameter': int_parameter1})

        await stream.wait_reply_received()
        assert stream.http_status == 200

        assert (await stream.receive_message()).int_parameter == int_parameter1
        assert (await stream.receive_message()).int_parameter == int_parameter1 + 1

        await stream.send_message({'int_parameter': int_parameter2})
        assert (await stream.receive_message()).int_parameter == int_parameter2 + 10
        assert (await stream.receive_message()).int_parameter == int_parameter2 + 11

        assert not await stream.receive_message()


@pytest.mark.asyncio
@pytest.mark.flows
async def test_stream_unary_success_flow(server_factory, client_factory):
    int_parameter1 = random.randint(0, 1000)
    int_parameter2 = int_parameter1 + 1

    class Handler(BaseHandler):
        async def stream_stream_method(self, stream):
            message1 = await stream.receive_message()
            message2 = await stream.receive_message()

            await stream.send_message({'int_parameter': message1.int_parameter + message2.int_parameter})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    reply = await client.stream_stream_method([
        {'int_parameter': int_parameter1},
        {'int_parameter': int_parameter2},
    ])

    assert reply.http_status == 200
    assert reply.messages[0].int_parameter == int_parameter1 + int_parameter2


@pytest.mark.asyncio
@pytest.mark.flows
async def test_stream_unary_success_flow_long_call(server_factory, client_factory):
    int_parameter1 = random.randint(0, 1000)
    int_parameter2 = int_parameter1 + 1

    class Handler(BaseHandler):
        async def stream_stream_method(self, stream):
            message1 = await stream.receive_message()
            message2 = await stream.receive_message()

            await stream.send_message({'int_parameter': message1.int_parameter + message2.int_parameter})

    server = await server_factory(Handler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    async with client.stream_stream_method.connect() as stream:
        await stream.send_message({'int_parameter': int_parameter1})
        await stream.send_message({'int_parameter': int_parameter2})

        await stream.wait_reply_received()
        assert stream.http_status == 200

        assert (await stream.receive_message()).int_parameter == int_parameter1 + int_parameter2
