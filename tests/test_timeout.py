import asyncio
import time

import pytest

from rebelrpc.exceptions import StreamTerminatedError
from rebelrpc.exceptions import ClientTimeoutError

from tests.services.dummy_service import BaseHandler
from tests.services.dummy_service import get_dummy_spec


@pytest.mark.asyncio
@pytest.mark.timeout
async def test_client_timeout(server_factory, client_factory):
    timeout = 0.1

    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.receive_message()
            await asyncio.sleep(100)

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    time_request_started = time.time()
    try:
        await client.unary_unary_method(timeout=timeout)
    except ClientTimeoutError:
        pass
    else:
        pytest.fail("Unary client did not get timeout error")

    request_duration = time.time() - time_request_started
    assert request_duration < 2 * timeout


@pytest.mark.asyncio
@pytest.mark.timeout
async def test_server_timeout(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await asyncio.sleep(100)

    timeout = 0.1
    server = await server_factory(Handler, get_dummy_spec(), timeout=timeout)

    client = client_factory(get_dummy_spec(), port=server.port)

    time_request_started = time.time()
    try:
        await client.unary_unary_method()
    except StreamTerminatedError:
        pass
    else:
        pytest.fail("Unary client did not get stream termination error")

    request_duration = time.time() - time_request_started
    assert request_duration < 2 * timeout
