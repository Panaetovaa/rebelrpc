import asyncio

import pytest

from rebelrpc.exceptions import BadStatusError
from rebelrpc.exceptions import StreamTerminatedError
from rebelrpc.exceptions import ClientTimeoutError

from tests.services.dummy_service import BaseHandler
from tests.services.dummy_service import get_dummy_spec
from tests.utils import wait_for


@pytest.mark.asyncio
@pytest.mark.gc
@pytest.mark.parametrize("method", [
    "unary_stream_method",
])
async def test_streams_are_closed_after_request_done(server_factory, client_factory, method):
    """ Check that all streams are closed when request is done on server.
    """
    class Handler(BaseHandler):
        pass

    setattr(Handler, method, _send_echo_reply)
    server = await server_factory(Handler, get_dummy_spec())

    clients = [
        client_factory(get_dummy_spec(), port=server.port) for _ in range(100)
    ]
    checks = await asyncio.gather(*[
        _send_echo_request(client, method, request_id) for request_id, client in enumerate(clients)
    ], loop=server.loop)

    assert all(checks)

    # Check that all all connections are open
    assert len(server.get_connections()) == len(clients)

    # Check that all streams are closed on server side
    for connection in server.get_connections():
        await wait_for(lambda: len(connection.streams) == 0)
        assert len(connection.streams) == 0

    for client in clients:
        await wait_for(lambda: len(client.protocol.streams) == 0)
        assert len(client.protocol.streams) == 0


@pytest.mark.asyncio
@pytest.mark.gc
async def test_streams_are_closed_after_server_unexpected_error(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.receive_message()
            raise RuntimeError

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        await client.unary_unary_method()
    except StreamTerminatedError:
        pass

    # Check that all all connections are open
    assert len(server.get_connections()) == 1

    # Check that all streams are closed on server side
    for connection in server.get_connections():
        await wait_for(lambda: len(connection.streams) == 0)
        assert len(connection.streams) == 0

    assert len(client.protocol.streams) == 0


@pytest.mark.asyncio
@pytest.mark.gc
async def test_streams_are_closed_after_server_regular_error(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.receive_message()
            await stream.send_error(400, {'code': 'CODE'})

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        await client.unary_unary_method()
    except BadStatusError:
        pass

    # Check that all all connections are open
    assert len(server.get_connections()) == 1

    # Check that all streams are closed on server side
    for connection in server.get_connections():
        await wait_for(lambda: len(connection.streams) == 0)
        assert len(connection.streams) == 0

    assert len(client.protocol.streams) == 0


@pytest.mark.asyncio
@pytest.mark.gc
async def test_streams_are_closed_after_client_timeout(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.receive_message()
            await asyncio.sleep(100)

    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        await client.unary_unary_method(timeout=0.1)
    except ClientTimeoutError:
        pass

    # Check that all all connections are open
    assert len(server.get_connections()) == 1

    # Check that all streams are closed on server side
    for connection in server.get_connections():
        await wait_for(lambda: len(connection.streams) == 0)
        assert len(connection.streams) == 0

    assert len(client.protocol.streams) == 0


@pytest.mark.asyncio
@pytest.mark.gc
async def test_streams_are_closed_after_server_timeout(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await asyncio.sleep(100)

    server = await server_factory(Handler, get_dummy_spec(), timeout=0.1)

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        await client.unary_unary_method()
    except StreamTerminatedError:
        pass

    # Check that all all connections are open
    assert len(server.get_connections()) == 1

    # Check that all streams are closed on server side
    for connection in server.get_connections():
        await wait_for(lambda: len(connection.streams) == 0)
        assert len(connection.streams) == 0

    assert len(client.protocol.streams) == 0


@pytest.mark.asyncio
@pytest.mark.gc
async def test_streams_are_closed_for_slow_clients(server_factory, client_factory):
    class Handler(BaseHandler):
        async def unary_unary_method(self, stream):
            await stream.receive_message()

    server = await server_factory(Handler, get_dummy_spec(), timeout=0.1)

    client = client_factory(get_dummy_spec(), port=server.port)

    try:
        async with client.unary_unary_method.connect(timeout=3) as stream:
            await asyncio.sleep(1)
            await stream.send_message({})

    except StreamTerminatedError:
        pass

    # Check that all all connections are open
    assert len(server.get_connections()) == 1

    # Check that all streams are closed on server side
    for connection in server.get_connections():
        await wait_for(lambda: len(connection.streams) == 0)
        assert len(connection.streams) == 0

    assert len(client.protocol.streams) == 0


@pytest.mark.asyncio
@pytest.mark.gc
@pytest.mark.parametrize("method", [
    "unary_unary_method",
])
async def test_connection_on_server_is_closed_when_client_is_closed(server_factory, client_factory, method):
    server = await server_factory(BaseHandler, get_dummy_spec())

    clients = []
    for _ in range(100):
        client = client_factory(get_dummy_spec(), port=server.port)
        clients.append(client)
        await getattr(client, method)()

    assert len(server.get_connections()) == len(clients)

    for client in clients:
        client.close()

    await wait_for(lambda: len(server.get_connections()) == 0)
    assert len(server.get_connections()) == 0


@pytest.mark.asyncio
@pytest.mark.gc
@pytest.mark.parametrize("method", [
    "unary_unary_method",
])
async def test_client_reconnects_after_connection_closed_on_client_side(
        server_factory, client_factory, method,
):
    class Handler(BaseHandler):
        pass

    setattr(Handler, method, _send_echo_reply)
    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    await getattr(client, method)({'int_parameter': 0})

    assert len(server.get_connections()) == 1

    client.close()

    await wait_for(lambda: len(server.get_connections()) == 0)
    assert len(server.get_connections()) == 0

    reply = await getattr(client, method)({'int_parameter': 1})
    assert reply.message.int_parameter == 1

    assert len(server.get_connections()) == 1


@pytest.mark.asyncio
@pytest.mark.gc
@pytest.mark.parametrize("method", [
    "unary_unary_method",
])
async def test_client_reconnects_after_connection_closed_on_server_side(
        server_factory, client_factory, method
):
    class Handler(BaseHandler):
        pass

    setattr(Handler, method, _send_echo_reply)
    server = await server_factory(Handler, get_dummy_spec())

    client = client_factory(get_dummy_spec(), port=server.port)
    await getattr(client, method)({'int_parameter': 0})

    assert len(server.get_connections()) == 1

    server.close()

    await wait_for(lambda: len(server.get_connections()) == 0)
    assert len(server.get_connections()) == 0

    setattr(Handler, method, _send_echo_reply)
    server = await server_factory(Handler, get_dummy_spec(), port=server.port)

    reply = await getattr(client, method)({'int_parameter': 1})
    assert reply.message.int_parameter == 1

    assert len(server.get_connections()) == 1


async def _send_echo_request(client, method, request_id):
    reply = await getattr(client, method)({'int_parameter': request_id})
    return reply.message.int_parameter == request_id


async def _send_echo_reply(self, stream):
    message = await stream.receive_message()
    await stream.send_message({'int_parameter': message.int_parameter})
