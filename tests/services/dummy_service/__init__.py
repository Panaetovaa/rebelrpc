import os.path
import rebelrpc


def _abspath(path):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), path)


PROTO_PATH = _abspath('proto/service.json')


def get_dummy_spec():
    return rebelrpc.parse_spec_file(PROTO_PATH)


class BaseHandler:
    async def unary_unary_method(self, stream):
        pass

    async def unary_stream_method(self, stream):
        pass

    async def stream_unary_method(self, stream):
        pass

    async def stream_stream_method(self, stream):
        pass
