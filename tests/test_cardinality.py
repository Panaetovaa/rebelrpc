import pytest

from rebelrpc.exceptions import CardinalityError

from tests.services.dummy_service import BaseHandler
from tests.services.dummy_service import get_dummy_spec


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", ["unary_stream_method", "stream_stream_method"])
async def test_streaming_server_can_send_multiple_messages(server_factory, client_factory, method):
    async def x_stream_method(self, stream):
        await stream.send_message({'str_parameter': 'a'})
        await stream.send_message({'str_parameter': 'b'})
        await stream.send_message({'str_parameter': 'c'})

    class Handler(BaseHandler):
        pass

    setattr(Handler, method, x_stream_method)

    server = await server_factory(Handler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    reply = await (getattr(client, method)())

    assert len(reply.messages) == 3
    assert reply.messages[0].str_parameter == 'a'
    assert reply.messages[0].str_parameter == 'a'
    assert reply.messages[0].str_parameter == 'a'


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", ["unary_unary_method", "stream_unary_method"])
async def test_unary_server_cant_send_more_than_one_message(server_factory, client_factory, method):
    cardinality_exception_raised = False

    class Handler(BaseHandler):
        pass

    async def x_unary_method(self, stream):
        nonlocal cardinality_exception_raised
        await stream.send_message({})
        try:
            await stream.send_message({})
        except CardinalityError:
            cardinality_exception_raised = True

    setattr(Handler, method, x_unary_method)

    server = await server_factory(Handler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    await (getattr(client, method)({}))
    assert cardinality_exception_raised


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", ["stream_unary_method", "stream_stream_method"])
async def test_streaming_client_can_send_multiple_messages(server_factory, client_factory, method):
    received_messages = []

    async def x_stream_method(self, stream):
        while True:
            message = await stream.receive_message()
            if not message:
                break

            received_messages.append(message)

    class Handler(BaseHandler):
        pass

    setattr(Handler, method, x_stream_method)

    server = await server_factory(Handler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    await (getattr(client, method)([
        {'str_parameter': 'a'}, {'str_parameter': 'b'}, {'str_parameter': 'c'}
    ]))
    assert len(received_messages) == 3
    assert received_messages[0].str_parameter == 'a'
    assert received_messages[1].str_parameter == 'b'
    assert received_messages[2].str_parameter == 'c'


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", ["unary_unary_method", "unary_stream_method"])
async def test_unary_client_cant_send_more_than_one_message(server_factory, client_factory, method):
    server = await server_factory(BaseHandler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    async with getattr(client, method).connect() as stream:
        await stream.send_message({})
        try:
            await stream.send_message({})
        except CardinalityError:
            pass
        else:
            pytest.fail(
                "Unary client was managed to send more than one message using context manager"
            )

    try:
        await (getattr(client, method)([{}, {}]))
    except CardinalityError:
        pass
    else:
        pytest.fail("Unary client was managed to send more than one message using __call__")


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", [
    "unary_stream_method", "stream_stream_method",
    "stream_unary_method", "unary_unary_method",
])
async def test_server_not_sending_message(server_factory, client_factory, method):
    server = await server_factory(BaseHandler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    reply = await (getattr(client, method)())
    assert len(reply.messages) == 0
    assert reply.message is None


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", [
    "unary_stream_method", "stream_stream_method",
    "stream_unary_method", "unary_unary_method",
])
async def test_server_not_sending_message_to_context_manager(server_factory, client_factory, method):
    server = await server_factory(BaseHandler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    async with getattr(client, method).connect() as stream:
        message = await stream.receive_message()
        assert message is None

        message = await stream.receive_message()
        assert message is None


@pytest.mark.asyncio
@pytest.mark.cardinality
@pytest.mark.parametrize("method", [
    "unary_stream_method", "stream_stream_method",
    "stream_unary_method", "unary_unary_method",
])
async def test_client_not_sending_message(server_factory, client_factory, method):
    received_messages = []

    async def x_method(self, stream):
        while True:
            message = await stream.receive_message()
            if not message:
                break

            received_messages.append(message)

    class Handler(BaseHandler):
        pass

    setattr(Handler, method, x_method)

    server = await server_factory(Handler, get_dummy_spec())
    client = client_factory(get_dummy_spec(), port=server.port)

    await (getattr(client, method)())
    assert len(received_messages) == 0
