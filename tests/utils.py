import asyncio

import time


async def wait_for(condition_func, timeout=3):
    t = time.time()

    while time.time() < t + timeout:
        if condition_func():
            break

        await asyncio.sleep(timeout / 4)
