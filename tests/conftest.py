import asyncio

from contextlib import suppress

import pytest

import rebelrpc


@pytest.fixture
async def server_factory(unused_tcp_port_factory):
    _created_servers = []

    async def _create(handler, spec, **kwargs):
        port = kwargs.pop('port', unused_tcp_port_factory())

        server = rebelrpc.Server(handler, spec, **kwargs)
        _created_servers.append(server)

        await server.start(port=port)
        return server

    yield _create
    for server in _created_servers:
        server.close()


@pytest.fixture
def client_factory():
    _created_clients = []

    def _create(*args, **kwargs):
        client = rebelrpc.Client(*args, **kwargs)

        _created_clients.append(client)

        return client

    yield _create
    for client in _created_clients:
        client.close()


@pytest.fixture(autouse=True)
async def teardown(server_factory):
    yield

    pending = asyncio.Task.all_tasks()
    for task in pending:
        task.cancel()
        with suppress(Exception):
            await task
